
%============================================================================
%============================================================================
top_dir = '/Users/whannah/Data/DYNAMO/RADAR/TOGA/';

testfile = strcat(top_dir,'test.nc');		% only for debug mode

idir = strcat(top_dir,'data_ET_mat/');		% input directory
odir = strcat(top_dir,'data_ET_nc/');		% output directory

%============================================================================
%============================================================================
ifiles = dir(cstrcat(idir,'REV*'));
nfiles = length(ifiles);

cd(idir)
load(ifiles(1).name)

who
exit

for f = 1:nfiles
	cd(idir)
	load(ifiles(f).name)

	who

	exit
end

%============================================================================
%============================================================================