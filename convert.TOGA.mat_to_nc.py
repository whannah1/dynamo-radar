#!/usr/bin/env python
#=========================================================================================
#   This script converts TOGA radar data in matlab format to NetCDF format
#
#   SciPy.io has a 'loadmat' function that is used for reading '.mat' files
#
#   The SciPy package is used for writing NetCDF files
#
#   files are on a 2 km x 2 km grid
#   REV_yyyymmdd_hhMM.mat  (2 km height)
#       conv/strat mask
#       rain rate
#       reflectivity 
#   ET_yyyymmdd_hhMM.mat
#       echo top heights
#
#    Mar, 2015  Walter Hannah       University of Miami
#=========================================================================================
import os
import glob
import datetime
import numpy as np
import scipy as sp
import numpy.ma as ma
import scipy.io as spio
import Nio
home = os.getenv("HOME") 
#=========================================================================================
#=========================================================================================
debug    = False    # writes to 'testfile' instead of to odir
verbose  = True     # verbose output for debugging

useNio = False      # use scipy or Nio for creating Netcdf file

top_dir = home+'/Data/DYNAMO/RADAR/TOGA/'
testfile = top_dir+'test.nc'    # only for debug mode
idir = top_dir+"raw_data_mat/"      # input directory
odir = top_dir+"raw_data_nc/"       # output directory

#define variables that will go into output NetCDF file
var_name = ["z10","cs","r"]
var_file = ["E"  ,"R" ,"R"]

nvar = len(var_name)
#=========================================================================================
#=========================================================================================
mval = 9.969209968386869e+36
#mval = -9999.
days_per_month = [31,28,31,30,31,30,31,31,30,31,30,31]

ntime = 24

rfiles = glob.glob(idir+'REV*')
efiles = glob.glob(idir+'ET*')

nfiles = len(rfiles)
if debug : nfiles = 2
line = "--------------------------------------------------------------------------------"
#=========================================================================================
# Print Some diagnostic stuff
#=========================================================================================
print line
print
print "Input Directory  : "+idir
print "Output Directory : "+odir
print
print "first files :"
print "    "+rfiles[0]
print "    "+efiles[0]
print
rfile = spio.loadmat(rfiles[0])['radar']
efile = spio.loadmat(efiles[0])['et']
print line
if verbose:
    print "REV file readme :"
    print rfile['readme'][0,0][0]
    print line
    print "ETH file readme :"
    print efile['readme'][0,0][0]
print line
#exit()
#=========================================================================================
# get lat/lon coords
#=========================================================================================
infile = spio.netcdf.netcdf_file(top_dir+"dims_TOGA.nc", 'r')
lat = infile.variables['lat']
lon = infile.variables['lon']
nlat = len(lat[:,0])
nlon = len(lon[0,:])
#=========================================================================================
# convert each .m file pair to a .nc file
#=========================================================================================
for f in range(0,nfiles):
    if debug:
        print "    rfile: "+rfiles[f]
        print "    efile: "+efiles[f]
    R = spio.loadmat(rfiles[f])['radar']
    E = spio.loadmat(efiles[f])['et']
    #---------------------------------------------------
    # get date for file string
    #---------------------------------------------------
    yr = int(E['datest'][0][0][0][:4])
    mo = int(E['mo'] [0][0][0])
    dd = int(E['dd'] [0][0][0])
    hh = int(E['hh'] [0][0][0])
    mm = int(E['mm'] [0][0][0])
    #---------------------------------------------------
    # Check for output NetCDF file
    #---------------------------------------------------
    #ofile = odir+"TOGA."+str(yr)+str(mo)+str(dd)+"_"+str(hh)+str(mm)+".nc"
    fmt = '02d'
    ofile = odir+"TOGA."+str(yr)+format(mo,fmt)+format(dd,fmt)+"_"+format(hh,fmt)+format(mm,fmt)+".nc"
    if debug: ofile = testfile
    if os.path.isfile(ofile): os.remove(ofile)
    if  useNio : outfile = Nio.open_file(ofile,"c")
    if ~useNio : outfile = spio.netcdf.netcdf_file(ofile, 'w')
    #outfile.history = "Created by Walter Hannah (@NCSU) on "+str(datetime.datetime.now())
    #---------------------------------------------------
    # write time information to output
    #---------------------------------------------------
    outfile.createDimension('time', 1)
    ftime = outfile.createVariable('time', 'd', ('time',))
    fyr   = outfile.createVariable('yr', 'int32', ('time',))
    fmo   = outfile.createVariable('mo', 'int32', ('time',))
    fdd   = outfile.createVariable('dd', 'int32', ('time',))
    fhh   = outfile.createVariable('hh', 'int32', ('time',))
    fmm   = outfile.createVariable('mm', 'int32', ('time',))

    fyr[:] = yr
    fmo[:] = mo
    fdd[:] = dd
    fhh[:] = hh
    fmm[:] = mm

    etime = datetime.datetime(1970,1,1)
    stime = datetime.datetime(yr, mo, dd, hh, mm)

    ftime[:] = ( (stime - etime).total_seconds() )
    ftime.units = "seconds since "+str(etime)
    
    #---------------------------------------------------
    # Create coordinate variables and write to NetCDF
    #---------------------------------------------------
    z = E['z0'] [0][0][:,0]
    y = E['y0'] [0][0][:,0]
    x = E['x0'] [0][0][:,0]

    outfile.createDimension('z', len(z))
    outfile.createDimension('y', len(y))
    outfile.createDimension('x', len(x))
    outfile.createDimension('lat', nlat)
    outfile.createDimension('lon', nlon)
    
    fZ = outfile.createVariable('z', 'd', ('z',))
    fY = outfile.createVariable('y', 'd', ('y',))
    fX = outfile.createVariable('x', 'd', ('x',))
    flat = outfile.createVariable('lat', 'd', ('lat',))
    flon = outfile.createVariable('lon', 'd', ('lon',))
    fZ[:] = z
    fY[:] = y
    fX[:] = x
    flat[:] = lat[:,0]
    flon[:] = lon[0,:]
    fZ.units = "km"
    fY.units = "km"
    fX.units = "km"
    flat.units = "degrees north"
    flon.units = "degrees east"
    #---------------------------------------------------
    #---------------------------------------------------
    for v in range(0,nvar):
        var = var_name[v]
        if var_file[v] == "E" : matfile = E
        if var_file[v] == "R" : matfile = R
        ftmp = outfile.createVariable(var, 'float32', ('time','lat','lon'))
        if var=="z10" : var_info = ["km"    ,"10dBz Echo Top Height"]
        if var=="cs"  : var_info = [""      ,"Convective/Stratiform"]
        if var=="r"   : var_info = ["mm/day","Rainrate"]
        ftmp.units          = var_info[0]
        ftmp.long_name      = var_info[1]
        ftmp._FillValue     = mval
        ftmp.missing_value  = mval
        tmp = np.array(matfile[var][0][0]).astype(float)
        tmp = np.where( np.isfinite(tmp), tmp, mval )
        nm = np.sum( tmp==mval ).astype(float)
        tmp = np.ma.masked_where( tmp==mval, tmp )
        tmp.fill_value = mval
        ftmp[:,:,:] = tmp
        
        nt = np.prod( np.shape(tmp) ).astype(float)
        #print "% missing: "+str(nm/nt*100.)
        #exit()
    fpct = outfile.createVariable("pctmiss", 'float32', ('time',))
    fpct[0] = nm/nt*100.
    #---------------------------------------------------
    # Close the NetCDF file
    #---------------------------------------------------
    outfile.close()
    if verbose: print "ofile: "+ofile
    #exit()
#=========================================================================================
#=========================================================================================

# ET files
#('datenu', 'O'), 
#('datest', 'O'), 
#('mo', 'O'), 
#('dd', 'O'), 
#('hh', 'O'), 
#('mm', 'O'), 
#('i10', 'O'), 
#('i20', 'O'), 
#('i30', 'O'), 
#('i40', 'O'), 
#('z10', 'O'), 
#('z20', 'O'), 
#('z30', 'O'), 
#('z40', 'O'), 
#('x0', 'O'), 
#('y0', 'O'), 
#('z0', 'O'), 
#('readme', 'O')

# REV files
#('mo', 'O'), 
#('dd', 'O'), 
#('hh', 'O'), 
#('mm', 'O'), 
#('az', 'O'), 
#('dz', 'O'), 
#('sw', 'O'), 
#('vr', 'O'), 
#('cs', 'O'), 
#('r', 'O'), 
#('x0', 'O'), 
#('y0', 'O'), 
#('z0', 'O'), 
#('mask', 'O'), 
#('readme', 'O')

#=========================================================================================
#=========================================================================================





