load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/calendar_decode2.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "x11"
	fig_file = "~/Research/DYNAMO/RADAR/plot.test.spol.v1"
	
	debug 		= True
;====================================================================================================
;====================================================================================================		
 	wks = gsn_open_wks(fig_type,fig_file)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@tmXTOn                          = False
		res@tmXBMinorOn						= False
		res@tmYLMinorOn						= False
		res@tmYRMinorOn						= False
		res@gsnLeftStringFontHeightF        = 0.015
		;res@gsnCenterStringFontHeightF    	= 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tiXAxisFontHeightF				= 0.015
		res@tiYAxisFontHeightF				= 0.015
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		;res@gsnLeftString                   = ""
		;res@gsnCenterString              	= ""
		;res@gsnRightString                  = ""
		;res@tmXBLabelAngleF					= -50.
		
		lres = res
		lres@xyDashPattern              = 0
		lres@xyLineThicknessF           = 3.
		lres@xyLineColor                = "black"
		
		cres = res
		cres@cnFillOn 				= True
		cres@cnLinesOn 				= False
		cres@cnFillMode				= "RasterFill"
		;cres@cnFillPalette			= "WhiteBlueGreenYellowRed"
		cres@cnFillPalette			= "tbrAvg1"
		cres@trYReverse				= True
		cres@cnLevelSelectionMode	= "ExplicitLevels"
;====================================================================================================		
;====================================================================================================
	;ifiles = systemfunc("ls -1 /data2/whannah/DYNAMO/RADAR/SPOL/spol_sur_data/*")
	;infile = addfiles(ifiles,"r")
	;ListSetType(infile, "join")
	
	ifile = "/data2/whannah/DYNAMO/RADAR/SPOL/gridded_survey_scans_2d/spol_radar.OND_2011_DYNAMO.nc"
	infile = addfile(ifile,"r")
	print(infile)
	
	t = 100
	
	time	= infile->time		(t)
	rain 	= infile->rain_rate	(t,:,:)
	csfr	= infile->cs_part	(t,:,:)
	dbz		= infile->dbz_2p5_km(t,:,:)
	
	eth10	= infile->top_10_dBZ(t,:,:)
	eth20	= infile->top_20_dBZ(t,:,:)
	eth30	= infile->top_30_dBZ(t,:,:)
	
	;rain@long_name 	= "rain rate"
	;csfr@long_name 	= "C/S"
	;dbz@long_name 	= ""
	;eth10@long_name	= ""
	;eth20@long_name	= ""
	;eth30@long_name	= ""
	
	stime = cd_calendar(time,-5)
	fmt = "%0.2i"
	yr = sprinti("%0.4i",stime(:,0))
	mn = sprinti(fmt,stime(:,1))
	dy = sprinti(fmt,stime(:,2))
	hr = sprinti(fmt,stime(:,3))
	mi = sprinti(fmt,stime(:,4))
	sc = sprinti(fmt,stime(:,5))
	date = yr+"-"+mn+"-"+dy+"  "+hr+":"+mi+":"+sc
	
;====================================================================================================
;====================================================================================================
	plot = new(4,graphic)
	
	plot(0) = gsn_csm_contour(wks,dbz	,cres)
	plot(2) = gsn_csm_contour(wks,eth10	,cres)
	plot(3) = gsn_csm_contour(wks,eth30	,cres)
	;plot(3) = gsn_csm_contour(wks,eth10	,cres)
	
		tres = cres
		tres@cnLevels	= (/0.5,1.5/)
	plot(1) = gsn_csm_contour(wks,csfr	,tres)
	delete(tres)

;====================================================================================================
;====================================================================================================
		pres = True
		;pres@gsnPanelYWhiteSpacePercent  		= 5
		pres@txString							= date
		pres@amJust                       		= "TopLeft"
		pres@gsnPanelFigureStringsFontHeightF	= 0.01
		pres@gsnPanelFigureStrings          	= (/"a","b","c","d","e","f"/)

	;layout = (/dimsizes(plot),1/)
	layout = (/2,2/)
	
	gsn_panel(wks,plot,layout,pres)

	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if
;====================================================================================================
;====================================================================================================
end
