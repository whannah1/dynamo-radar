    DEGTORAD = 0.017453292519943
	xcenter  = 0.0
	ycenter  = 0.0	
	radius = 150.
;---------------------------------------------------------
;---------------------------------------------------------
function cfrLoadTime (ifile)
local infile
begin
	infile = addfile(ifile,"r")
	time = ut_calendar(infile->time,0)
	return time
end
;---------------------------------------------------------
;---------------------------------------------------------
function LoadDBZ (ifile)
local infile,idbz,rng,rsi,num_t,num_r,dbz
begin
	infile = addfile(ifile,"r")
	if .not.isfilevar(infile,"DZ") 				then return -1 end if
	if .not.isfilevar(infile,"ray_start_index") 	then return -1 end if
	if .not.isfilevar(infile,"ray_n_gates") 		then return -1 end if
	idbz    = short2flt(infile->DZ)
  	rng		= infile->ray_n_gates
  	rsi		= infile->ray_start_index
  	
	num_t = dimsizes(infile->time)
	num_r = dimsizes(infile->range)
	  
	dbz = new((/num_t,num_r/),float)
	do t = 0,num_t-1
	  dbz(t,:rng(t)-1) = idbz(rsi(t):rsi(t)+rng(t)-1)
	end do
	  
	return dbz
end
;---------------------------------------------------------
;---------------------------------------------------------
function LoadAngles (ifile)
local infile,angles
begin
  	infile = addfile(ifile,"r")
  	angles       = infile->azimuth
    angles(0:63) = angles(0:63)-360     ; fix to make angles monotonic
    return angles
end 
;---------------------------------------------------------
;---------------------------------------------------------
function LoadTime (ifile)
local infile,time
begin
  	infile = addfile(ifile,"r")
  	time = infile->time
    return time
end 
;---------------------------------------------------------
;---------------------------------------------------------
function LoadRange (ifile)
local infile,range
begin
  	infile = addfile(ifile,"r")
  	range = infile->range
    return range
end 
;---------------------------------------------------------
;---------------------------------------------------------
function createX (ifile)
local infile,x,xdims,angles,xarr,num_r,num_t
begin
	num_r 	 = dimsizes(LoadRange(ifile))
	num_t	 = dimsizes(LoadTime(ifile))
  	x        = radius / (num_r - 1) * ispan(0,num_r-1,1) 
  	xdims    = (/ num_t , num_r /)
  	angles   = conform(new(xdims,float),LoadAngles(ifile),0)
  	x2d      = conform(new(xdims,float),x,1)
  	xarr     =  xcenter + x2d  * cos(DEGTORAD * angles)
  return xarr
end
;---------------------------------------------------------
;---------------------------------------------------------
function createY (ifile)
local infile,x,xdims,angles,yarr,num_r,num_t
begin
	num_r 	 = dimsizes(LoadRange(ifile))
	num_t	 = dimsizes(LoadTime(ifile))
  	x        = radius / (num_r - 1) * ispan(0,num_r-1,1) 
	xdims    = (/ num_t , num_r /)
  	angles   = conform(new(xdims,float),LoadAngles(ifile),0)
  	x2d      = conform(new(xdims,float),x,1)
  	yarr     =  ycenter + x2d  * sin(DEGTORAD * angles)
  return yarr
end 
;---------------------------------------------------------
;---------------------------------------------------------
;function radar_plot (dbz)
;local
;begin
;end
;---------------------------------------------------------
;---------------------------------------------------------
