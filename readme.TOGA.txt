--------------------------------------------------------------------------------
REV file readme :
NASA/TOGA C-band Doppler radar - DYNAMO 2011
Elizabeth J. Thompson liz@atmos.colostate.edu

Convective/stratiform rain partitioning and rainfall estimation done at 1.25 km AGL or level #2
Followed Yuter and Houze 1998 methodology exactly
background = NaN; strat = 0.9; conv = 1.1

Gridded using RadX2Grid (NCAR)
dx and dy horizontal grid spacing = 2000 m
dz vertical grid spacing = 500 m
max range = 150 km

VARIABLES:
MO/DD/HH/MM = date, time
AZ = radar reflectivity corrected for attenuation and anomalous propagation
DZ = raw radar reflectivity
SW = spectrum width (m/s)
VR = radial velocity (m/s)
CS = conv/stratiform partitioning
R = rainfall rate (mm/hr)
lat0/lon0 = 2D grid of coordinates
x0, y0, z0  = 1D array of x, y, and z coordinates in AGL or km about the origin = radar

NETCDF data had weird orientation relative to MATLAB,
checked with raw polar coord data. So I flipped everything
so that the plotting command could simply be:
pcolor(x0,y0,AZ(:,:,2);
or even
pcolor(AZ(:,:,2));



--------------------------------------------------------------------------------
ETH file readme :
NASA/TOGA C-band Doppler radar - DYNAMO 2011-2012
Elizabeth J. Thompson liz@atmos.colostate.edu


Statistics of radar data:

volume: vol

datenu	= date number (matlab)
datest	= date string (matlab)
mo	= month
dd	= day
hh	= hour
mm	= month

(year 	= 2011)

i10	= maximum level (#: 1-20) of echoes >= 10 dBZ
z10  	= maximum height (km) of echoes >= 10 dBZ

and same for 20, 30, and 40 dBZ.

z0 =

    0.5000
    1.0000
    1.5000
    2.0000
    2.5000
    3.0000
    3.5000
    4.0000
    4.5000
    5.0000
    5.5000
    6.0000
    6.5000
    7.0000
    7.5000
    8.0000
    8.5000
    9.0000
    9.5000
   10.0000
   10.5000
   11.0000
   11.5000
   12.0000
   12.5000
   13.0000
   13.5000
   14.0000
   14.5000
   15.0000
   15.5000
   16.0000
   16.5000
   17.0000
   17.5000
   18.0000
   18.5000
   19.0000
   19.5000
   20.0000

about 40 missing files due to data issues: /data/liz/TOGA/GRID_plots/missing_files.txt

--------------------------------------------------------------------------------
