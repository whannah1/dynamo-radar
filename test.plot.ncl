load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
begin 

	fig_type = "png"
	fig_file = "test.cfrad.toga"

	;ifile  = "to1111124090001.RAW2Z6J.cdf"
	ifile  = "cfrad.20111124_090006_000_toga_v1_PPI.nc.nc"
	infile = addfile(ifile,"r")
;=======	======================================================================
;=======	======================================================================
  idbz      = short2flt(infile->DZ)
  rng		= infile->ray_n_gates
  rsi		= infile->ray_start_index

  time  = infile->time
  range = infile->range
  num_t = dimsizes(time)
  num_r = dimsizes(range)
  
  dbz = new((/num_t,num_r/),float)
  dsizes    = dimsizes(dbz)
  do t = 0,num_t-1
    dbz(t,:rng(t)-1) = idbz(rsi(t):rsi(t)+rng(t)-1)
  end do
  
  angles       = infile->azimuth
  angles(0:63) = angles(0:63)-360     ; fix to make angles monotonic
  
  DEGTORAD = 0.017453292519943
  xcenter  = 0.0
  ycenter  = 0.0
  ;radius   = 960 * 0.25 ; this is radius in kilometers
  radius = 150.

;=======	======================================================================
; Create 2D coordinate arrays.
;=======	======================================================================
  inc      = radius / (dsizes(1) - 1)
  x        = inc * ispan(0,dsizes(1)-1,1) 
  angles2d = conform(dbz,angles,0)
  x2d      = conform(dbz,x,1)
  xarr     =  xcenter + x2d  * cos(DEGTORAD * angles2d)
  yarr     =  ycenter + x2d  * sin(DEGTORAD * angles2d)
;=======	======================================================================
;=======	======================================================================
  wks = gsn_open_wks(fig_type,fig_file)
  gsn_define_colormap(wks,"rainbow")
	  cnres                   = True
	  cnres@gsnMaximize       = True
	  cnres@sfXArray          = xarr
	  cnres@sfYArray          = yarr
	  cnres@gsnSpreadColors   = True
	  cnres@cnFillOn          = True
	  cnres@cnLinesOn         = False
	  cnres@cnFillMode        = "RasterFill"      ; this mode is fastest
	  cnres@trGridType        = "TriangularMesh"
	  cnres@lbLabelAutoStride = True

  contour = gsn_csm_contour(wks,dbz,cnres)
  
  	print("")
  	print("	"+fig_file+"."+fig_type)
  	print("")
  
;=======	======================================================================
;=======	======================================================================
end
